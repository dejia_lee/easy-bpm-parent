package com.pig.easy.bpm;

import org.flowable.engine.ProcessEngine;
import org.flowable.engine.runtime.ProcessInstance;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * todo:
 *
 * @author : pig
 * @date : 2020/6/19 11:39
 */
@SpringBootTest
public class StartProcessTest {

    @Autowired
    ProcessEngine processEngine;

    @Test
    void StartProcess(){
        ProcessInstance processInstance = processEngine.getRuntimeService().startProcessInstanceById("pig_test1:8:393982676880310272");

        System.out.println("processInstance = " + processInstance);
    }

}
