package com.pig.easy.bpm.mapper;

import com.pig.easy.bpm.dto.request.RoleGroupRoleDetailQueryDTO;
import com.pig.easy.bpm.dto.response.RoleDTO;
import com.pig.easy.bpm.dto.response.RoleGroupRoleDetailDTO;
import com.pig.easy.bpm.entity.RoleDO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 * 角色表 Mapper 接口
 * </p>
 *
 * @author pig
 * @since 2020-06-14
 */
@Mapper
public interface RoleMapper extends BaseMapper<RoleDO> {

    List<RoleDTO> getListByCondiction(RoleDO roleDO);

    Integer updateByRoleCode(RoleDO roleDO);

    List<RoleGroupRoleDetailDTO> getRoleGroupRoleDetailByCondition(RoleGroupRoleDetailQueryDTO queryDTO);

    Integer getCountByCondition(RoleGroupRoleDetailQueryDTO queryDTO);
}
