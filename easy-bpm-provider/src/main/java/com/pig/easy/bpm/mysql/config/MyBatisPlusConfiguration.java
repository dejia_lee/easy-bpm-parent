package com.pig.easy.bpm.mysql.config;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.autoconfigure.MybatisPlusProperties;
import com.baomidou.mybatisplus.core.MybatisConfiguration;
import com.baomidou.mybatisplus.core.config.GlobalConfig;
import com.baomidou.mybatisplus.core.injector.ISqlInjector;
import com.baomidou.mybatisplus.core.parser.ISqlParser;
import com.baomidou.mybatisplus.extension.injector.LogicSqlInjector;
import com.baomidou.mybatisplus.extension.parsers.BlockAttackSqlParser;
import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import com.baomidou.mybatisplus.extension.plugins.PerformanceInterceptor;
import com.baomidou.mybatisplus.extension.plugins.SqlExplainInterceptor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

/**
 * 功能描述: Mybatis 多租户配置, 感觉 mybatisPlus 获取 租户ID 有问题，不合适，建议不用该租户方案
 *
 *
 * @author : pig
 * @since  : 2020/5/14 13:47
 */
@Slf4j
@AutoConfigureAfter(MybatisPlusProperties.class)
public class MyBatisPlusConfiguration {

    /** 多租户字字段 */
    private static final String SYSTEM_TENANT_ID = "tenant_id";
    /** 不参与添加多租户表 */
//    private static final List<String> IGNORE_TENANT_TABLES = new ArrayList("bpm_user","bpm_company");

    private static final String USER_TENANT_PREFIX = "best:bpm:tenantId:";

    @Autowired
    MybatisPlusProperties mybatisPlusProperties;


    @Bean
    @ConditionalOnMissingBean
    public PaginationInterceptor paginationInterceptor() {
        PaginationInterceptor paginationInterceptor = new PaginationInterceptor();
       // paginationInterceptor.setDialectType(DbType.MYSQL.getDb());

//        // 创建SQL解析器集合
//        List<ISqlParser> sqlParserList = new ArrayList<>();
//        // 创建租户SQL解析器
//        TenantSqlParser tenantSqlParser = new TenantSqlParser();
//        // 设置租户处理器
//        tenantSqlParser.setTenantHandler(new TenantHandler() {
//            @Override
//            public Expression getTenantId() {
//                // 设置当前租户ID，实际情况你可以从cookie、或者缓存中拿都行
//                return new StringValue("jiannan");
//            }
//
//            @Override
//            public String getTenantIdColumn() {
//                return SYSTEM_TENANT_ID;
//            }
//            @Override
//            public boolean doTableFilter(String tableName) {
//                return IGNORE_TENANT_TABLES.stream().anyMatch((e) -> e.equalsIgnoreCase(tableName));
//            }
//        });
//
//        sqlParserList.add(tenantSqlParser);
//        paginationInterceptor.setSqlParserList(sqlParserList);
        return paginationInterceptor;
    }

//    @Bean
//    ConfigurationCustomizer mybatisConfigurationCustomizer() {
//        return new ConfigurationCustomizer() {
//            @Override
//            public void customize(MybatisConfiguration configuration) {
//                configuration.addInterceptor(new com.github.pagehelper.PageInterceptor());
//            }
//        };
//    }



    /**
     * SQL 执行效率插件
     * 设置 dev test 环境开启
     */
    @Bean
    @Profile( {"local", "dev", "test"})
    @ConditionalOnMissingBean
    public PerformanceInterceptor performanceInterceptor() {
        PerformanceInterceptor performanceInterceptor = new PerformanceInterceptor();
        performanceInterceptor.setMaxTime(10000);
        performanceInterceptor.setFormat(true);
        return performanceInterceptor;
    }

    @Bean
    public ISqlInjector sqlInjector() {
        return new LogicSqlInjector();
    }

    @Bean
    @Profile( {"local", "dev", "test"})
    @ConditionalOnMissingBean
    public SqlExplainInterceptor sqlExplainInterceptor(){
        SqlExplainInterceptor sqlExplainInterceptor = new SqlExplainInterceptor();
        List<ISqlParser> sqlParserList = new ArrayList<>();
        sqlParserList.add(new BlockAttackSqlParser());
        sqlExplainInterceptor.setSqlParserList(sqlParserList);

        return sqlExplainInterceptor;
    }

    @PostConstruct
    public void setMybatisPlusProperties() {
        // 设置mapper路径
        mybatisPlusProperties.setMapperLocations(new String[] {"classpath*:/mapper/*Mapper.xml", "classpath*:/mapper/**/*Mapper.xml"});
        // 设置关闭下划线，关闭大写，主键自增长
        GlobalConfig globalConfig = new GlobalConfig();

        GlobalConfig.DbConfig dbConfig = new GlobalConfig.DbConfig();
        dbConfig.setCapitalMode(false);
        dbConfig.setIdType(IdType.AUTO);
        dbConfig.setTableUnderline(true);

        globalConfig.setDbConfig(dbConfig);

        mybatisPlusProperties.setGlobalConfig(globalConfig);

        MybatisConfiguration mybatisConfiguration = new MybatisConfiguration();
        mybatisConfiguration.setMapUnderscoreToCamelCase(true);
        //配置slq打印日志
       // mybatisConfiguration.setLogImpl(org.apache.ibatis.logging.stdout.StdOutImpl.class);

        mybatisPlusProperties.setConfiguration(mybatisConfiguration);

    }


}
