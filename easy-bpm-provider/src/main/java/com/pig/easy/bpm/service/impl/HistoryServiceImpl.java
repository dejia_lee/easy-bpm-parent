package com.pig.easy.bpm.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.pig.easy.bpm.dto.request.HistorySaveDTO;
import com.pig.easy.bpm.dto.response.ApplyDTO;
import com.pig.easy.bpm.dto.response.HistoryDTO;
import com.pig.easy.bpm.entity.HistoryDO;
import com.pig.easy.bpm.entityError.EntityError;
import com.pig.easy.bpm.mapper.HistoryMapper;
import com.pig.easy.bpm.service.ApplyService;
import com.pig.easy.bpm.service.HistoryService;
import com.pig.easy.bpm.utils.BeanUtils;
import com.pig.easy.bpm.utils.BestBpmAsset;
import com.pig.easy.bpm.utils.CommonUtils;
import com.pig.easy.bpm.utils.Result;
import org.apache.commons.lang3.StringUtils;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 审批历史表 服务实现类
 * </p>
 *
 * @author pig
 * @since 2020-07-01
 */
@com.alibaba.dubbo.config.annotation.Service
public class HistoryServiceImpl extends BeseServiceImpl<HistoryMapper, HistoryDO> implements HistoryService {

    @Autowired
    HistoryMapper historyMapper;
    @Reference
    @Lazy
    ApplyService applyService;

    @Override
    public Result<List<HistoryDTO>> getListByApplyId(Long applyId) {

        BestBpmAsset.isAssetEmpty(applyId);

        Result<List<ApplyDTO>> result1 = applyService.getChildrenListByApplyId(applyId);
        if (result1.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
            return Result.responseError(result1.getEntityError());
        }
        List<Long> applyList = new ArrayList<>();
        applyList.add(applyId);
        for (ApplyDTO applyDTO : result1.getData()) {
            if (!applyList.contains(applyDTO.getApplyId())) {
                applyList.add(applyDTO.getApplyId());
            }
        }
        QueryWrapper<HistoryDO> queryWrapper = new QueryWrapper<>(HistoryDO.builder().validState(VALID_STATE).build())
                .in("apply_id", applyList)
                .orderByDesc("apply_id","history_id");
        List<HistoryDO> list = historyMapper.selectList(queryWrapper);
        if (list == null) {
            list = new ArrayList<>();
        }
        List<HistoryDTO> result = new ArrayList<>();
        HistoryDTO historyDTO = null;
        for (HistoryDO historyDO : list) {
            historyDTO = BeanUtils.switchToDTO(historyDO, HistoryDTO.class);
            result.add(historyDTO);
        }

        return Result.responseSuccess(result);
    }

    @Override
    public Result<HistoryDTO> getHistoryByTaskId(Long taskId) {

        BestBpmAsset.isAssetEmpty(taskId);
        HistoryDO historyDO = historyMapper.selectOne(new QueryWrapper<>(HistoryDO.builder().taskId(taskId).build()));
        if (historyDO == null) {
            return Result.responseError(EntityError.DATA_NOT_FOUND_ERROR);
        }
        HistoryDTO historyDTO = BeanUtils.switchToDTO(historyDO, HistoryDTO.class);

        return Result.responseSuccess(historyDTO);
    }

    @Override
    public Result<Integer> addHistory(HistorySaveDTO historySaveDTO) {

        BestBpmAsset.isAssetEmpty(historySaveDTO);
        BestBpmAsset.isAssetEmpty(historySaveDTO.getApplyId());
        BestBpmAsset.isAssetEmpty(historySaveDTO.getApproveOpinion());

        HistoryDO historyDO = BeanUtils.switchToDO(historySaveDTO, HistoryDO.class);
        int insert = historyMapper.insert(historyDO);

        return Result.responseSuccess(insert);
    }

    @Override
    public Result<Integer> updateHistoryById(HistorySaveDTO historySaveDTO) {

        BestBpmAsset.isAssetEmpty(historySaveDTO);
        BestBpmAsset.isAssetEmpty(historySaveDTO.getHistoryId());

        HistoryDO historyDO = BeanUtils.switchToDO(historySaveDTO, HistoryDO.class);
        int num = historyMapper.updateById(historyDO);
        return Result.responseSuccess(num);
    }

    @Override
    public Result<Integer> insertHistory(Long applyId, String tenantId, Long taskId, String taskName, Long approverUserId, String approveRealName, String approveActionCode, String approveOpinion,String systemCode, String paltform) {

        BestBpmAsset.isAssetEmpty(applyId);
        BestBpmAsset.isAssetEmpty(tenantId);
        BestBpmAsset.isAssetEmpty(approverUserId);
        BestBpmAsset.isAssetEmpty(approveRealName);
        BestBpmAsset.isAssetEmpty(approveOpinion);

        HistoryDO historyDO = HistoryDO.builder().applyId(applyId).tenantId(tenantId)
                .approveUserId(approverUserId).approveRealName(approveRealName).approveOpinion(approveOpinion)
                .operatorId(approverUserId).operatorName(approveRealName).build();
        if (CommonUtils.evalLong(taskId) > 0) {
            historyDO.setTaskId(taskId);
        }
        if (!StringUtils.isEmpty(taskName)) {
            historyDO.setTaskName(taskName);
        }
        if (!StringUtils.isEmpty(approveActionCode)) {

            historyDO.setApproveActionCode(approveActionCode);
            // 这里需要放入MAP获取
            historyDO.setApproveActionName(approveActionCode);
        }
        if (!StringUtils.isEmpty(systemCode)) {
            historyDO.setSystem(systemCode);
        }
        if (!StringUtils.isEmpty(paltform)) {
            historyDO.setPaltform(paltform);
        }
        int insert = historyMapper.insert(historyDO);
        return Result.responseSuccess(insert);
    }
}
