package com.pig.easy.bpm.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.pig.easy.bpm.dto.request.MessageContentQueryDTO;
import com.pig.easy.bpm.dto.request.MessageContentSaveOrUpdateDTO;
import com.pig.easy.bpm.dto.response.MessageContentDTO;
import com.pig.easy.bpm.entity.MessageContentDO;
import com.pig.easy.bpm.entityError.EntityError;
import com.pig.easy.bpm.mapper.MessageContentMapper;
import com.pig.easy.bpm.service.MessageContentService;
import com.pig.easy.bpm.utils.BeanUtils;
import com.pig.easy.bpm.utils.CommonUtils;
import com.pig.easy.bpm.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
/**
 * <p>
 * 通知内容表 服务实现类
 * </p>
 *
 * @author pig
 * @since 2020-10-20
 */
@org.apache.dubbo.config.annotation.Service
public class MessageContentServiceImpl extends BeseServiceImpl<MessageContentMapper, MessageContentDO>implements MessageContentService {

        @Autowired
        MessageContentMapper mapper;

        @Override
        public Result<PageInfo<MessageContentDTO>>getListByCondition(MessageContentQueryDTO param){

          if(param==null){
            return Result.responseError(EntityError.ILLEGAL_ARGUMENT_ERROR);
          }
          int pageIndex=CommonUtils.evalInt(param.getPageIndex(),DEFAULT_PAGE_INDEX);
          int pageSize=CommonUtils.evalInt(param.getPageSize(),DEFAULT_PAGE_SIZE);

          PageHelper.startPage(pageIndex,pageSize);
          param.setValidState(VALID_STATE);
          List<MessageContentDTO>list=mapper.getListByCondition(param);
          if(list==null){
           list=new ArrayList<>();
          }
          PageInfo<MessageContentDTO>pageInfo=new PageInfo<>(list);
          return Result.responseSuccess(pageInfo);
        }


        @Override
        public Result<Integer>insertMessageContent(MessageContentSaveOrUpdateDTO param){

          if(param==null){
            return Result.responseError(EntityError.ILLEGAL_ARGUMENT_ERROR);
          }

          MessageContentDO temp=BeanUtils.switchToDO(param, MessageContentDO.class);
            Integer num=mapper.insert(temp);
            return Result.responseSuccess(num);
        }

         @Override
         public Result<Integer>updateMessageContent(MessageContentSaveOrUpdateDTO param){

          if(param==null){
           return Result.responseError(EntityError.ILLEGAL_ARGUMENT_ERROR);
          }

          MessageContentDO temp=BeanUtils.switchToDO(param, MessageContentDO.class);
          Integer num=mapper.updateById(temp);
          return Result.responseSuccess(num);
        }

        @Override
        public Result<Integer>deleteMessageContent(MessageContentSaveOrUpdateDTO param){

          if(param==null){
             return Result.responseError(EntityError.ILLEGAL_ARGUMENT_ERROR);
          }

          MessageContentDO temp=BeanUtils.switchToDO(param, MessageContentDO.class);
          temp.setValidState(INVALID_STATE);
          Integer num=mapper.updateById(temp);
          return Result.responseSuccess(num);
        }

}
