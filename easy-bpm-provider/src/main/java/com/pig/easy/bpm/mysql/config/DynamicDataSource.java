package com.pig.easy.bpm.mysql.config;

import com.pig.easy.bpm.mysql.reister.DynamicDataSourceContextHolder;
import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

/**
 * todo:  查询数据库前动态获取DataSource
 *
 * @author : pig
 * @date : 2020/5/9 10:57
 */
public class DynamicDataSource extends AbstractRoutingDataSource {

    @Override
    protected Object determineCurrentLookupKey() {
        return DynamicDataSourceContextHolder.getDataSourceType();
    }
}
