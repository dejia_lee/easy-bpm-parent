package com.pig.easy.bpm.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.pig.easy.bpm.dto.request.VariableDictQueryDTO;
import com.pig.easy.bpm.dto.request.VariableDictSaveOrUpdateDTO;
import com.pig.easy.bpm.dto.response.ProcessDTO;
import com.pig.easy.bpm.dto.response.VariableDictDTO;
import com.pig.easy.bpm.entity.VariableDictDO;
import com.pig.easy.bpm.entityError.EntityError;
import com.pig.easy.bpm.mapper.VariableDictMapper;
import com.pig.easy.bpm.service.ProcessService;
import com.pig.easy.bpm.service.VariableDictService;
import com.pig.easy.bpm.utils.BeanUtils;
import com.pig.easy.bpm.utils.CommonUtils;
import com.pig.easy.bpm.utils.Result;
import org.apache.commons.lang3.StringUtils;
import org.apache.dubbo.config.annotation.Reference;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * <p>
 * 变量表 服务实现类
 * </p>
 *
 * @author pig
 * @since 2020-07-09
 */
@Service
public class VariableDictServiceImpl extends BeseServiceImpl<VariableDictMapper, VariableDictDO> implements VariableDictService {

    @Autowired
    VariableDictMapper mapper;
    @Reference
    ProcessService processService;

    @Override
    public Result<PageInfo<VariableDictDTO>> getListByCondition(VariableDictQueryDTO param) {

        if (param == null) {
            return Result.responseError(EntityError.ILLEGAL_ARGUMENT_ERROR);
        }
        int pageIndex = CommonUtils.evalInt(param.getPageIndex(), DEFAULT_PAGE_INDEX);
        int pageSize = CommonUtils.evalInt(param.getPageSize(), DEFAULT_PAGE_SIZE);
        List<Long> processIdList = new ArrayList<>(Arrays.asList(0L));
        if (!StringUtils.isEmpty(param.getProcessKey())) {
            Result<ProcessDTO> result = processService.getProcessByProcessKey(param.getProcessKey());
            if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
                return Result.responseError(result.getEntityError());
            }
            processIdList.add(result.getData().getProcessId());
            param.setProcessKey(null);
        }
        if(CommonUtils.evalLong(param.getProcessId()) > 0){
            processIdList.add(param.getProcessId());
            param.setProcessId(null);
        }
        param.setProcessIdList(processIdList);

        PageHelper.startPage(pageIndex, pageSize);
        param.setValidState(VALID_STATE);
        List<VariableDictDTO> list = mapper.getListByCondition(param);
        if (list == null) {
            list = new ArrayList<>();
        }
        PageInfo<VariableDictDTO> pageInfo = new PageInfo<>(list);
        return Result.responseSuccess(pageInfo);
    }


    @Override
    public Result<Integer> insertVariableDict(VariableDictSaveOrUpdateDTO param) {

        if (param == null) {
            return Result.responseError(EntityError.ILLEGAL_ARGUMENT_ERROR);
        }

        VariableDictDO temp = BeanUtils.switchToDO(param, VariableDictDO.class);
        Integer num = mapper.insert(temp);
        return Result.responseSuccess(num);
    }

    @Override
    public Result<Integer> updateVariableDict(VariableDictSaveOrUpdateDTO param) {

        if (param == null) {
            return Result.responseError(EntityError.ILLEGAL_ARGUMENT_ERROR);
        }

        VariableDictDO temp = BeanUtils.switchToDO(param, VariableDictDO.class);
        Integer num = mapper.updateById(temp);
        return Result.responseSuccess(num);
    }

    @Override
    public Result<Integer> deleteVariableDict(VariableDictSaveOrUpdateDTO param) {

        if (param == null) {
            return Result.responseError(EntityError.ILLEGAL_ARGUMENT_ERROR);
        }

        VariableDictDO temp = BeanUtils.switchToDO(param, VariableDictDO.class);
        temp.setValidState(INVALID_STATE);
        Integer num = mapper.updateById(temp);
        return Result.responseSuccess(num);
    }

}
