package com.pig.easy.bpm.config;

import com.pig.easy.bpm.flowable.BpmProcessDiagramGenerator;
import com.pig.easy.bpm.flowable.CustomHistoricActivityInstanceEntityManager;
import com.pig.easy.bpm.flowable.FlowableDeployCache;
import com.pig.easy.bpm.handler.CustomUserTaskParseHandler;
import com.pig.easy.bpm.listener.GlobalFlowableEventListener;
import com.pig.easy.bpm.utils.SnowKeyGen;
import org.flowable.common.engine.api.delegate.event.FlowableEventListener;
import org.flowable.common.engine.impl.history.HistoryLevel;
import org.flowable.engine.*;
import org.flowable.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.flowable.engine.parse.BpmnParseHandler;
import org.flowable.spring.ProcessEngineFactoryBean;
import org.flowable.spring.SpringProcessEngineConfiguration;
import org.flowable.spring.boot.EngineConfigurationConfigurer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author pigs
 */
@Configuration
public class FlowableConfig implements EngineConfigurationConfigurer<SpringProcessEngineConfiguration> {

    @Autowired
    private FlowableDeployCache customFlowableDeployCache;

   @Autowired
    private PlatformTransactionManager transactionManager;
   @Autowired
    private DataSource dataSource;

    @Autowired
    private GlobalFlowableEventListener globalFlowableEventListener;

    @Override
    public void configure(SpringProcessEngineConfiguration springProcessEngineConfiguration) {

        springProcessEngineConfiguration.setDataSource(dataSource);
        springProcessEngineConfiguration.setAsyncExecutorActivate(false);
        springProcessEngineConfiguration.setDatabaseSchemaUpdate(ProcessEngineConfiguration.DB_SCHEMA_UPDATE_TRUE);
        springProcessEngineConfiguration.setDatabaseType(ProcessEngineConfiguration.DATABASE_TYPE_MYSQL);
        springProcessEngineConfiguration.setHistoryLevel(HistoryLevel.FULL);

        List<FlowableEventListener> flowableEventListenerList = new ArrayList<>();
        flowableEventListenerList.add(globalFlowableEventListener);
        springProcessEngineConfiguration.setEventListeners(flowableEventListenerList);

        List<BpmnParseHandler> bpmnParseHandlers = new ArrayList<>();
        bpmnParseHandlers.add(new CustomUserTaskParseHandler());
       // bpmnParseHandlers.add(new BpmBpmnParseHandler());
        springProcessEngineConfiguration.setCustomDefaultBpmnParseHandlers(bpmnParseHandlers);

        //流程图字体
        springProcessEngineConfiguration.setActivityFontName("宋体");
        springProcessEngineConfiguration.setAnnotationFontName("宋体");
        springProcessEngineConfiguration.setLabelFontName("宋体");

        springProcessEngineConfiguration.setIdGenerator(new SnowKeyGen());

        springProcessEngineConfiguration.setProcessDefinitionCache(customFlowableDeployCache);

        springProcessEngineConfiguration.setTransactionManager(transactionManager);

        springProcessEngineConfiguration.setProcessDiagramGenerator(new BpmProcessDiagramGenerator());
        springProcessEngineConfiguration.setHistoricActivityInstanceEntityManager(new CustomHistoricActivityInstanceEntityManager(springProcessEngineConfiguration,springProcessEngineConfiguration.getHistoricActivityInstanceDataManager()));
    }


    /**
     * 流程引擎，与spring整合使用factoryBean
     * @param processEngineConfiguration
     * @return
     */
    @Bean
    @Primary
    public ProcessEngineFactoryBean processEngine(ProcessEngineConfiguration processEngineConfiguration){
        ProcessEngineFactoryBean processEngineFactoryBean = new ProcessEngineFactoryBean();
        processEngineFactoryBean.setProcessEngineConfiguration((ProcessEngineConfigurationImpl) processEngineConfiguration);
        return processEngineFactoryBean;
    }

    @Bean("repositoryService1")
    public RepositoryService repositoryService(ProcessEngine processEngine) {
        return processEngine.getRepositoryService();
    }

    @Bean("runtimeService1")
    public RuntimeService runtimeService(ProcessEngine processEngine){
        return processEngine.getRuntimeService();
    }

    @Bean("formService1")
    public FormService formService(ProcessEngine processEngine){
        return processEngine.getFormService();
    }

    @Bean("identityService1")
    public IdentityService identityService(ProcessEngine processEngine){
        return processEngine.getIdentityService();
    }

    @Bean("taskService1")
    public TaskService taskService(ProcessEngine processEngine){
        return processEngine.getTaskService();
    }

    @Bean("historyService1")
    public HistoryService historyService(ProcessEngine processEngine){
        return  processEngine.getHistoryService();
    }

    @Bean("managementService1")
    public ManagementService managementService(ProcessEngine processEngine){
        return processEngine.getManagementService();
    }


}
