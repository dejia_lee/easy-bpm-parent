package com.pig.easy.bpm.dto.request;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * todo:
 *
 * @author : pig
 * @date : 2020/5/16 15:20
 */
@Data
@ToString
@EqualsAndHashCode(callSuper = false)
public class AddUserDTO extends BaseRequestDTO implements Serializable {

    private static final long serialVersionUID = -3789525603848436912L;

    private String id;

    private Long userId;

    private String userName;

    private String password;

    private String realName;

    private String email;

    private String phone;

    private Integer gender;

    private LocalDateTime birthDate;

    private Long companyId;

    private Long deptId;

    private String tenantId;

    private LocalDateTime entryTime;

    private LocalDateTime leaveTime;

    private Integer hireStatus;

    private Integer validState;

    private Long operatorId;

    private String operatorName;

    private String assessToken;

    private String positionCode;

    private String positionName;
}
