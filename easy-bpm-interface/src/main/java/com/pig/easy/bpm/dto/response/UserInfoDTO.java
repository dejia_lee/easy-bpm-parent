package com.pig.easy.bpm.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * todo:
 *
 * @author : pig
 * @date : 2020/5/14 15:46
 */
@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class UserInfoDTO extends BaseResponseDTO implements Serializable {

    private static final long serialVersionUID = 8421845399600521410L;

    private String id;

    private Long userId;

    private String userName;

    private String realName;

    private String email;

    private String phone;

    private Integer gender;

    private String avatar;

    private Date birthDate;

    private Long companyId;

    private Long deptId;

    private String tenantId;

    private Date entryTime;

    private Date leaveTime;

    private Integer hireStatus;

    private Long operatorId;

    private String operatorName;

    private String accessToken;

    private String system;

    private String paltform;

    private String companyCode;

    private String companyName;

    private String deptName;

    private String deptCode;

    private List<ListDTO> companyList;

    private List<ListDTO> deptList;

    private List<ListDTO> roleList;

    private String positionCode;

    private String positionName;


}
