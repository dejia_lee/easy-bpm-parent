package com.pig.easy.bpm.service;

import com.github.pagehelper.PageInfo;
import com.pig.easy.bpm.dto.request.DeptQueryDTO;
import com.pig.easy.bpm.dto.request.DeptSaveOrUpdateDTO;
import com.pig.easy.bpm.dto.response.DeptDTO;
import com.pig.easy.bpm.dto.response.ItemDTO;
import com.pig.easy.bpm.dto.response.TreeDTO;
import com.pig.easy.bpm.utils.Result;

import java.util.List;

/**
 * <p>
 * 部门表 服务类
 * </p>
 *
 * @author pig
 * @since 2020-05-20
 */
public interface DeptService {

    Result<PageInfo<DeptDTO>> getListByCondition(DeptQueryDTO deptQueryDTO);

    Result<List<DeptDTO>> getList(DeptQueryDTO deptQueryDTO);

    Result<DeptDTO> getDeptByDeptId(Long deptId);

    Result<DeptDTO> getDeptByDeptCode(String deptCode);

    Result<List<ItemDTO>> getDeptItemList(DeptQueryDTO deptQueryDTO);

    Result<List<TreeDTO>> getDeptTree(Long parentDeptId, String tenantId);

    Result<List<TreeDTO>> getDeptTree(Long parentDeptId,Long compantId, String tenantId);

    Result<List<TreeDTO>> getOrganTree(String tenantId, Long compantId);

    Result<List<TreeDTO>> getDeptTree(String tenantId);

    Result<Integer> insertDept(DeptSaveOrUpdateDTO deptSaveOrUpdateDTO);

    Result<Integer> updateDept(DeptSaveOrUpdateDTO deptSaveOrUpdateDTO);

}
