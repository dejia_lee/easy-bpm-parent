package com.pig.easy.bpm.event;

/**
 * todo:
 *
 * @author : pig
 * @date : 2020/5/20 15:07
 */
public enum BestBpmEventType {

    ENTITY_CREATED,

    ENTITY_UPDATED;

}
