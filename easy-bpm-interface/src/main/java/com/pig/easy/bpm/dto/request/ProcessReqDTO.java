package com.pig.easy.bpm.dto.request;

import lombok.Data;
import lombok.ToString;

/**
 * todo:
 *
 * @author : pig
 * @date : 2020/6/6 10:12
 */
@Data
@ToString
public class ProcessReqDTO extends BaseRequestDTO {

    private static final long serialVersionUID = -6928315436074178397L;

    private Long processId;

    private String processKey;

    private String processName;

    private Long processMenuId;

    private String processAbbr;

    private Long companyId;

    private String companyCode;

    private String tenantId;

    private Long processDetailId;

    private Integer sort;

    private Integer processType;

    private Integer processStatus;

    private String remarks;

    private Integer validState;

    private Long operatorId;

    private String operatorName;

    private Integer pageIndex;

    private Integer pageSize;
}
