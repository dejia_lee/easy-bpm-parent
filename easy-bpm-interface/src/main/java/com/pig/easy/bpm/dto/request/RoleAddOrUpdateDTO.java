package com.pig.easy.bpm.dto.request;

import lombok.Data;
import lombok.ToString;

/**
 * todo:
 *
 * @author : pig
 * @date : 2020/6/14 14:02
 */
@Data
@ToString
public class RoleAddOrUpdateDTO extends BaseRequestDTO {

    private static final long serialVersionUID = -8963386409179712737L;

    private Long roleId;

    private String roleCode;

    private String roleName;

    private Integer roleLevel;

    private String roleAbbr;

    private String roleAliasName;

    private String tenantId;

    private Long companyId;

    private Long deptId;

    private String remarks;

    private Integer validState;

    private Long operatorId;

    private String operatorName;
}
