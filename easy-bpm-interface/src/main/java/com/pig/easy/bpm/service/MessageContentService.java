package com.pig.easy.bpm.service;

import com.pig.easy.bpm.dto.request.*;
import com.pig.easy.bpm.dto.response.*;
import com.github.pagehelper.PageInfo;
import com.pig.easy.bpm.utils.Result;
/**
 * <p>
 * 通知内容表 服务类
 * </p>
 *
 * @author pig
 * @since 2020-10-20
 */
public interface MessageContentService {

        Result<PageInfo<MessageContentDTO>> getListByCondition(MessageContentQueryDTO param);

        Result<Integer> insertMessageContent(MessageContentSaveOrUpdateDTO param);

        Result<Integer> updateMessageContent(MessageContentSaveOrUpdateDTO param);

        Result<Integer> deleteMessageContent(MessageContentSaveOrUpdateDTO param);

 }
