package com.pig.easy.bpm.event;

import com.pig.easy.bpm.entityError.EntityError;
import com.pig.easy.bpm.execption.BpmException;
import javafx.event.Event;
import javafx.event.EventDispatchChain;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * todo:
 *
 * @author : pig
 * @date : 2020/5/20 15:12
 */
@Slf4j
public class BestBpmEventDispatcherImpl implements BestBpmEventDispatcher {

    /* 全局事件监听器 */
    private List<BestBpmEventListener> eventListeners;
    /* 类型事件监听器 */
    private Map<BestBpmEventType, List<BestBpmEventListener>> typedListeners;

    private boolean enabled = true;

    public BestBpmEventDispatcherImpl() {
        eventListeners = new CopyOnWriteArrayList<>();
        typedListeners = new ConcurrentHashMap<>();
    }

    @Override
    public void addEventListener(BestBpmEventListener listenerToAdd) {
        if (listenerToAdd == null) {
            throw new BpmException(new EntityError(EntityError.ILLEGAL_ARGUMENT_ERROR.getCode(), "Listener cannot be null."));
        }
        if (!eventListeners.contains(listenerToAdd)) {
            eventListeners.add(listenerToAdd);
        }
    }

    @Override
    public void addEventListener(BestBpmEventListener listenerToAdd, BestBpmEventType... types) {
        if (listenerToAdd == null) {
            throw new BpmException(new EntityError(EntityError.ILLEGAL_ARGUMENT_ERROR.getCode(), "Listener cannot be null."));
        }

        if (types == null || types.length == 0) {
            addEventListener(listenerToAdd);

        } else {
            for (BestBpmEventType type : types) {
                addTypedEventListener(listenerToAdd, type);
            }
        }
    }

    @Override
    public void removeEventListener(BestBpmEventListener listenerToRemove) {
        eventListeners.remove(listenerToRemove);

        for (List<BestBpmEventListener> listeners : typedListeners.values()) {
            listeners.remove(listenerToRemove);
        }
    }

    @Override
    public void dispatchEvent(BestBpmEvent event) {

        if (event == null
                || event.getBpmSourceDTO().getEventType() == null) {
            throw new BpmException(new EntityError(EntityError.ILLEGAL_ARGUMENT_ERROR.getCode(), "Event cannot be null."));
        }

        // Call global listeners
        if (!eventListeners.isEmpty()) {
            for (BestBpmEventListener listener : eventListeners) {
                dispatchEvent(event, listener);
            }
        }

        // Call typed listeners, if any
        List<BestBpmEventListener> typed = typedListeners.get(event.getBpmSourceDTO().getEventType());
        if (typed != null && !typed.isEmpty()) {
            for (BestBpmEventListener listener : typed) {
                dispatchEvent(event, listener);
            }
        }
    }

    @Override
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    private void dispatchEvent(BestBpmEvent event, BestBpmEventListener listener) {
        dispatchNormalEventListener(event, listener);
    }

    private void dispatchNormalEventListener(BestBpmEvent event, BestBpmEventListener listener) {
        try {
            listener.onEvent(event);
        } catch (Throwable t) {
            // Ignore the exception and continue notifying remaining listeners. The listener
            // explicitly states that the exception should not bubble up
            log.warn("Exception while executing event-listener, which was ignored", t);
        }
    }

    private synchronized void addTypedEventListener(BestBpmEventListener listener, BestBpmEventType type) {
        List<BestBpmEventListener> listeners = typedListeners.get(type);
        if (listeners == null) {
            // Add an empty list of listeners for this type
            listeners = new CopyOnWriteArrayList<>();
            typedListeners.put(type, listeners);
        }

        if (!listeners.contains(listener)) {
            listeners.add(listener);
        }
    }

    @Override
    public Event dispatchEvent(Event event, EventDispatchChain tail) {
        return null;
    }
}
