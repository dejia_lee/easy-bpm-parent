package com.pig.easy.bpm.service;

import com.github.pagehelper.PageInfo;
import com.pig.easy.bpm.dto.request.RoleAddOrUpdateDTO;
import com.pig.easy.bpm.dto.request.RoleGroupRoleDetailQueryDTO;
import com.pig.easy.bpm.dto.request.RoleReqDTO;
import com.pig.easy.bpm.dto.response.ItemDTO;
import com.pig.easy.bpm.dto.response.RoleDTO;
import com.pig.easy.bpm.dto.response.RoleGroupRoleDetailDTO;
import com.pig.easy.bpm.utils.Result;

import java.util.List;

/**
 * <p>
 * 角色表 服务类
 * </p>
 *
 * @author pig
 * @since 2020-06-14
 */
public interface RoleService {

    Result<PageInfo<RoleDTO>> getListByCondiction(RoleReqDTO roleReqDTO);

    Result<List<RoleDTO>> getList(RoleReqDTO roleReqDTO);

    Result<PageInfo<RoleGroupRoleDetailDTO>> getRoleGroupRoleDetailByCondition(RoleGroupRoleDetailQueryDTO queryDTO);

    Result<List<RoleGroupRoleDetailDTO>> getRoleGroupRoleDetailList(RoleGroupRoleDetailQueryDTO queryDTO);

    Result<RoleDTO> getRole(RoleReqDTO roleReqDTO);

    Result<RoleDTO> getRoleByRoleCode(String roleCode);

    Result<RoleDTO> getRoleByRoleId(Long roleId);

    Result<Integer> insertRole(RoleAddOrUpdateDTO roleAddOrUpdateDTO);

    Result<Integer> updateRoleByRoleCode(RoleAddOrUpdateDTO roleAddOrUpdateDTO);

    Result<List<ItemDTO>> getRoleDictByTenantId(String tenantId);
}
