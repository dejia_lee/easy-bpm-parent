package com.pig.easy.bpm.event;

import lombok.extern.slf4j.Slf4j;

/**
 * todo:
 *
 * @author : zhoulin.zhu
 * @date : 2020/10/29 16:39
 */
@Slf4j
public class BestBpmEventListenerImpl implements BestBpmEventListener {

    private BpmSourceDTO bpmSourceDTO;

    @Override
    public void onEvent(BestBpmEvent event) {

        if (event == null) {
            log.debug("even must not be null !");
            throw new RuntimeException("even must not be null !");
        }
        bpmSourceDTO = event.getBpmSourceDTO();

        switch (BestBpmEventType.valueOf(bpmSourceDTO.getEventType().name())){

            case ENTITY_CREATED:
                break;

            case ENTITY_UPDATED:
                break;

            default:
                break;
        }
    }
}
