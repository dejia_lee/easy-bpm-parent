package com.pig.easy.bpm.controller;


import com.alibaba.dubbo.config.annotation.Reference;
import com.github.pagehelper.PageInfo;
import com.pig.easy.bpm.constant.BpmConstant;
import com.pig.easy.bpm.dto.request.DeptQueryDTO;
import com.pig.easy.bpm.dto.request.DictQueryDTO;
import com.pig.easy.bpm.dto.request.DictUpdateDTO;
import com.pig.easy.bpm.dto.response.ItemDTO;
import com.pig.easy.bpm.entityError.EntityError;
import com.pig.easy.bpm.service.*;
import com.pig.easy.bpm.utils.JsonResult;
import com.pig.easy.bpm.utils.Result;
import com.pig.easy.bpm.vo.request.DictQueryVO;
import com.pig.easy.bpm.vo.request.DictSaveOrUpdateVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 字典表 前端控制器
 * </p>
 *
 * @author pig
 * @since 2020-05-28
 */
@RestController
@RequestMapping("/dict")
@Api(tags = "字典管理", value = "字典管理")
public class DictController extends BaseController {

    @Reference
    DictService dictService;
    @Reference
    DeptService deptService;
    @Reference
    RoleGroupService roleGroupService;
    @Reference
    RoleService roleService;
    @Reference
    ProcessService processService;

    @ApiOperation(value = "查询字典列表", notes = "查询字典列表", produces = "application/json")
    @PostMapping("/getList")
    public JsonResult getList(@ApiParam(name = "字典信息", value = "传入json格式", required = true) @Valid @RequestBody DictQueryVO dictQueryVO) {

        if(dictQueryVO == null
                || StringUtils.isEmpty(dictQueryVO.getTenantId())){
            return JsonResult.error(EntityError.ILLEGAL_ARGUMENT_ERROR);
        }
        DictQueryDTO dictQueryDTO = switchToDTO(dictQueryVO, DictQueryDTO.class);
        Result<PageInfo> result = dictService.getListByCondition(dictQueryDTO);
        if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
            return JsonResult.error(result.getEntityError());
        }
        return JsonResult.success(result.getData());
    }

    @ApiOperation(value = "新增或修改字典", notes = "新增或修改字典", produces = "application/json")
    @PostMapping("/insertOrUpdateDict")
    public JsonResult insertOrUpdateDict(@ApiParam(name = "字典信息", value = "传入json格式", required = true) @Valid @RequestBody DictSaveOrUpdateVO dictSaveOrUpdateVO) {

        if(dictSaveOrUpdateVO == null
                || StringUtils.isEmpty(dictSaveOrUpdateVO.getTenantId())
                || StringUtils.isEmpty(dictSaveOrUpdateVO.getDictCode())){
            return JsonResult.error(EntityError.ILLEGAL_ARGUMENT_ERROR);
        }
        DictUpdateDTO dictQueryDTO = switchToDTO(dictSaveOrUpdateVO, DictUpdateDTO.class);
        Result<Integer> result = dictService.insertOrUpdateDict(dictQueryDTO);
        if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
            return JsonResult.error(result.getEntityError());
        }
        return JsonResult.success(result.getData());
    }

    @ApiOperation(value = "删除字典", notes = "删除字典", produces = "application/json")
    @PostMapping("/delete")
    public JsonResult delete(@ApiParam(name = "字典信息", value = "传入json格式", required = true) @Valid @RequestBody DictSaveOrUpdateVO dictSaveOrUpdateVO) {

        if(dictSaveOrUpdateVO == null
                || StringUtils.isEmpty(dictSaveOrUpdateVO.getTenantId())
                || StringUtils.isEmpty(dictSaveOrUpdateVO.getDictCode())){
            return JsonResult.error(EntityError.ILLEGAL_ARGUMENT_ERROR);
        }
        DictUpdateDTO dictQueryDTO = switchToDTO(dictSaveOrUpdateVO, DictUpdateDTO.class);
        dictQueryDTO.setValidState(BpmConstant.INVALID_STATE);
        Result<Integer> result = dictService.insertOrUpdateDict(dictQueryDTO);
        if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
            return JsonResult.error(result.getEntityError());
        }
        return JsonResult.success(result.getData());
    }

    @ApiOperation(value = "查询角色字典", notes = "查询角色字典", produces = "application/json")
    @PostMapping("/getRoleDictByTenantId/{tenantId}")
    public JsonResult getRoleDictByTenantId(@ApiParam(required = true, name = "租户编号", value = "tenantId", example = "pig") @PathVariable("tenantId") String tenantId) {

        if(StringUtils.isEmpty(tenantId)){
            return JsonResult.error(EntityError.ILLEGAL_ARGUMENT_ERROR);
        }
        Result<List<ItemDTO>> result = roleService.getRoleDictByTenantId(tenantId);
        if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
            return JsonResult.error(result.getEntityError());
        }
        return JsonResult.success(result.getData());
    }

    @ApiOperation(value = "查询角色组字典", notes = "查询角色组字典", produces = "application/json")
    @PostMapping("/getRoleGroupDictByTenantId/{tenantId}")
    public JsonResult getRoleGroupDictByTenantId(@ApiParam(required = true, name = "租户编号", value = "tenantId", example = "pig") @PathVariable("tenantId") String tenantId) {

        if(StringUtils.isEmpty(tenantId)){
            return JsonResult.error(EntityError.ILLEGAL_ARGUMENT_ERROR);
        }
        Result<List<ItemDTO>> result = roleGroupService.getRoleGroupDict(tenantId);
        if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
            return JsonResult.error(result.getEntityError());
        }
        return JsonResult.success(result.getData());
    }

    @ApiOperation(value = "查询部门字典", notes = "查询部门字典", produces = "application/json")
    @PostMapping("/getDeptListByTenantId/{tenantId}")
    public JsonResult getDeptListByTenantId(@ApiParam(required = true, name = "租户编号", value = "tenantId", example = "pig") @PathVariable("tenantId") String tenantId) {

        if(StringUtils.isEmpty(tenantId)){
            return JsonResult.error(EntityError.ILLEGAL_ARGUMENT_ERROR);
        }
        DeptQueryDTO deptQueryDTO = new  DeptQueryDTO();
        deptQueryDTO.setTenantId(tenantId);
        Result<List<ItemDTO>> result = deptService.getDeptItemList(deptQueryDTO);
        if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
            return JsonResult.error(result.getEntityError());
        }
        return JsonResult.success(result.getData());
    }

    @ApiOperation(value = "查询字典值", notes = "查询字典值", produces = "application/json")
    @PostMapping("/getDictListByDictCode/{dictCode}")
    public JsonResult getDictListByDictCode(
            @ApiParam(required = true, name = "字典编码", value = "dictCode", example = "pig:dict:processStatus") @PathVariable("dictCode") String dictCode) {

        if(StringUtils.isEmpty(dictCode)){
            return JsonResult.error(EntityError.ILLEGAL_ARGUMENT_ERROR);
        }

        Result<List<ItemDTO>> result = dictService.getDictListByDictCode(dictCode);
        if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
            return JsonResult.error(result.getEntityError());
        }
        return JsonResult.success(result.getData());
    }

    @ApiOperation(value = "获取流程列表字典", notes = "获取流程列表字典", produces = "application/json")
    @PostMapping("/getProcessDict/{tenantId}")
    public JsonResult getProcessDict(@ApiParam(required = true, name = "租户编号", value = "tenantId", example = "pig") @PathVariable("tenantId") String tenantId) {

        if(StringUtils.isEmpty(tenantId)){
            return JsonResult.error(EntityError.ILLEGAL_ARGUMENT_ERROR);
        }
        Result<List<ItemDTO>> result = processService.getProcessDict(tenantId);
        if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
            return JsonResult.error(result.getEntityError());
        }
        return JsonResult.success(result.getData());
    }


}

