package com.pig.easy.bpm.vo.request;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 通知白名单
 * </p>
 *
 * @author pig
 * @since 2020-10-20
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)

@ApiModel(value="通知白名单对象", description="通知白名单")
public class MessageWhiteListQueryVO implements Serializable {

    private static final long serialVersionUID=1L;

        
                    @ApiModelProperty(value = "编号")
             /**
    * 编号
    */
        private String whiteId;

    
                    @ApiModelProperty(value = "租户编号")
             /**
    * 租户编号
    */
        private String tenantId;

    
                    @ApiModelProperty(value = "作用于流程编号， 0为所有流程")
             /**
    * 作用于流程编号， 0为所有流程
    */
        private Integer processId;

    
                    @ApiModelProperty(value = "白名单类型 ALL：所有，APPROVE：审批，OVERTIME：超时")
             /**
    * 白名单类型 ALL：所有，APPROVE：审批，OVERTIME：超时
    */
        private String whiteTypeCode;

    
                    @ApiModelProperty(value = "白名单")
             /**
    * 白名单
    */
        private String whiteList;

    
                    @ApiModelProperty(value = "排序")
             /**
    * 排序
    */
        private Integer order;

    
                    @ApiModelProperty(value = "备注")
             /**
    * 备注
    */
        private String remarks;

    
                    @ApiModelProperty(value = "状态 1 有效 0 失效")
             /**
    * 状态 1 有效 0 失效
    */
        private Integer validState;

    
                    @ApiModelProperty(value = "操作人工号")
             /**
    * 操作人工号
    */
        private Integer operatorId;

    
                    @ApiModelProperty(value = "操作人姓名")
             /**
    * 操作人姓名
    */
        private String operatorName;

    
                    @ApiModelProperty(value = "创建时间")
             /**
    * 创建时间
    */
        private LocalDateTime createTime;

    
                    @ApiModelProperty(value = "更新时间")
             /**
    * 更新时间
    */
        private LocalDateTime updateTime;
    /**
    *  当前页码
    */
    private Integer pageIndex;

    /**
     * 每页展示数量
     */
    private Integer pageSize;


}
