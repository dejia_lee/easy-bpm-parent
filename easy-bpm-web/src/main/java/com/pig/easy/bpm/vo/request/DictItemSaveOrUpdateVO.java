package com.pig.easy.bpm.vo.request;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.io.Serializable;

/**
 * todo:
 *
 * @author : pig
 * @date : 2020/7/2 20:50
 */
@Data
@ToString
@EqualsAndHashCode(callSuper = false)
public class DictItemSaveOrUpdateVO implements Serializable {

    private static final long serialVersionUID = 2607784059020890655L;

    private Long itemId;

    private Long dictId;

    private String itemValue;

    private String tenantId;

    private String itemText;

    private Integer sort;

    private String remark;

    private Integer validState;
}
