package com.pig.easy.bpm.controller;


import com.alibaba.dubbo.config.annotation.Reference;
import com.github.pagehelper.PageInfo;
import com.pig.easy.bpm.constant.BpmConstant;
import com.pig.easy.bpm.dto.request.FormReqDTO;
import com.pig.easy.bpm.entityError.EntityError;
import com.pig.easy.bpm.service.FormService;
import com.pig.easy.bpm.utils.BestBpmAsset;
import com.pig.easy.bpm.utils.JsonResult;
import com.pig.easy.bpm.utils.Result;
import com.pig.easy.bpm.vo.request.FormVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author pig
 * @since 2020-05-28
 */
@RestController
@Api(tags = "表单管理", value = "表单管理")
@RequestMapping("/form")
public class FormController extends BaseController {

    @Reference
    FormService formService;

    @ApiOperation(value = "查询表单列表", notes = "查询表单列表",  produces = "application/json")
    @PostMapping("/getList")
    public JsonResult getList(@Valid @RequestBody FormVO formVO) {

        BestBpmAsset.isAssetEmpty(formVO);
        BestBpmAsset.isAssetEmpty(formVO.getTenantId());
        FormReqDTO formDTO = switchToDTO(formVO, FormReqDTO.class);

        Result<PageInfo>  result = formService.getListByCondition(formDTO);
        if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
            return JsonResult.error(result.getEntityError());
        }
        return JsonResult.success(result.getData());
    }

    @ApiOperation(value = "新增表单", notes = "新增表单",  produces = "application/json")
    @PostMapping("/insertForm")
    public JsonResult insertForm(@Valid @RequestBody FormVO formVO) {

        BestBpmAsset.isAssetEmpty(formVO);
        BestBpmAsset.isAssetEmpty(formVO.getTenantId());
        BestBpmAsset.isAssetEmpty(formVO.getFormKey());
        FormReqDTO formDTO = switchToDTO(formVO, FormReqDTO.class);
        formDTO.setOperatorId(currentUserInfo().getUserId());
        formDTO.setOperatorName(currentUserInfo().getRealName());

        Result<Integer>  result = formService.insertForm(formDTO);
        if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
            return JsonResult.error(result.getEntityError());
        }
        return JsonResult.success(result.getData());
    }

    @ApiOperation(value = "修改表单", notes = "修改表单",  produces = "application/json")
    @PostMapping("/updateForm")
    public JsonResult updateForm(@Valid @RequestBody FormVO formVO) {

        BestBpmAsset.isAssetEmpty(formVO);
        BestBpmAsset.isAssetEmpty(formVO.getTenantId());
        BestBpmAsset.isAssetEmpty(formVO.getFormKey());
        FormReqDTO formDTO = switchToDTO(formVO, FormReqDTO.class);

        Result<Integer>  result = formService.updateFormByFormKey(formDTO);
        if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
            return JsonResult.error(result.getEntityError());
        }
        return JsonResult.success(result.getData());
    }

    @ApiOperation(value = "删除表单", notes = "删除表单",  produces = "application/json")
    @PostMapping("/deleteForm")
    public JsonResult deleteForm(@Valid @RequestBody FormVO formVO) {

        BestBpmAsset.isAssetEmpty(formVO);
        BestBpmAsset.isAssetEmpty(formVO.getFormKey());
        FormReqDTO formDTO = switchToDTO(formVO, FormReqDTO.class);
        formDTO.setOperatorId(currentUserInfo().getUserId());
        formDTO.setOperatorName(currentUserInfo().getRealName());
        formDTO.setValidState(BpmConstant.INVALID_STATE);

        Result<Integer>  result = formService.updateFormByFormKey(formDTO);
        if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
            return JsonResult.error(result.getEntityError());
        }
        return JsonResult.success(result.getData());
    }
}

