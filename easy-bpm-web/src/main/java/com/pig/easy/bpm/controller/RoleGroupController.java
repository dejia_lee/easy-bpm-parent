package com.pig.easy.bpm.controller;


import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 角色组 前端控制器
 * </p>
 *
 * @author pig
 * @since 2020-06-14
 */
@RestController
@RequestMapping("/roleGroup")
@Api(tags = "角色组详细信息", value = "角色组详细信息")
public class RoleGroupController extends BaseController {


}

